# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 12:24:05 2020

@author: Hezi
"""


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.signal import savgol_filter
from tkinter.filedialog import askopenfilename
filename1 = askopenfilename()
spectrumData1=pd.read_csv(filename1, names=["Wavelength", "SiPM1", "SiPM2", "SiPM3", "SiPM4", "SiPM5", "SiPM6", "SiPM7", "SiPM8","SiPM9","SiPM10","SiPM11", "SiPM12", "SiPM13", "SiPM14", "SiPM15", "SiPM16", "SiPM17", "SiPM18","SiPM19","SiPM20","SiPM21", "SiPM22", "SiPM23", "SiPM24", "SiPM25", "SiPM26", "SiPM27","SiPM28", "SiPM29", "SiPM30", "SiPM31","SiPM32","SiPM33", "SiPM34", "SiPM35", "SiPM36"],header=None)

SiPm1=pd.to_numeric(spectrumData1.SiPM1[1:-1])
SiPm2=pd.to_numeric(spectrumData1.SiPM2[1:-1])
SiPm3=pd.to_numeric(spectrumData1.SiPM3[1:-1])
SiPm4=pd.to_numeric(spectrumData1.SiPM4[1:-1])
SiPm5=pd.to_numeric(spectrumData1.SiPM5[1:-1])
SiPm6=pd.to_numeric(spectrumData1.SiPM6[1:-1])
SiPm7=pd.to_numeric(spectrumData1.SiPM7[1:-1])
SiPm8=pd.to_numeric(spectrumData1.SiPM8[1:-1])
SiPm9=pd.to_numeric(spectrumData1.SiPM9[1:-1])
SiPm10=pd.to_numeric(spectrumData1.SiPM9[1:-1])

Wavelength=pd.to_numeric(spectrumData1.Wavelength[1:-1])

plt.rc('xtick',labelsize=24) 
plt.rc('ytick',labelsize=24)
plt.figure(334),plt.plot(Wavelength,SiPm1,Wavelength,SiPm2,Wavelength,SiPm3,Wavelength,SiPm4,Wavelength,SiPm5,Wavelength,SiPm6,Wavelength,SiPm7,Wavelength,SiPm8,Wavelength,SiPm9,Wavelength,SiPm10,linestyle='-')
csfont = {'fontname':'Times New Roman'}
plt.title(filename1[-20:-1],**csfont,fontsize=24) 
plt.xlabel('$\lambda$ [nm]',**csfont,fontsize=24)
plt.ylabel('Intensity [a.u.]',**csfont,fontsize=24)

#%%
SiPm1=pd.to_numeric(spectrumData1.SiPM17[1:-1])
SiPm2=pd.to_numeric(spectrumData1.SiPM18[1:-1])
SiPm3=pd.to_numeric(spectrumData1.SiPM19[1:-1])
SiPm4=pd.to_numeric(spectrumData1.SiPM20[1:-1])
SiPm5=pd.to_numeric(spectrumData1.SiPM21[1:-1])
SiPm6=pd.to_numeric(spectrumData1.SiPM22[1:-1])
SiPm7=pd.to_numeric(spectrumData1.SiPM23[1:-1])
SiPm8=pd.to_numeric(spectrumData1.SiPM24[1:-1])
SiPm9=pd.to_numeric(spectrumData1.SiPM25[1:-1])
SiPm10=pd.to_numeric(spectrumData1.SiPM28[1:-1])
#SiPm11=pd.to_numeric(spectrumData1.SiPM27[1:-1])
plt.figure(564),plt.plot(Wavelength,SiPm1,Wavelength,SiPm2,Wavelength,SiPm3,Wavelength,SiPm4,Wavelength,SiPm5,Wavelength,SiPm6,Wavelength,SiPm7,Wavelength,SiPm8,Wavelength,SiPm9,Wavelength,SiPm10,linestyle=':',color='b')
csfont = {'fontname':'Times New Roman'}
plt.title(filename1[-20:-1],**csfont,fontsize=24) 
plt.xlabel('$\lambda$ [nm]',**csfont,fontsize=24)
plt.ylabel('Intensity [a.u.]',**csfont,fontsize=24)
plt.legend(loc=0, prop={'size': 32})
# plt.title('YagOptics solid line- Nikon dashed line',**csfont,fontsize=24)